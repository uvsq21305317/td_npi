package td_npi;

public class MissingNumberException extends Exceptions {
	
	public MissingNumberException() 
	{
		super("Chiffre manquant.");
	}

}
