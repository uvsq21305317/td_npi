package td_npi;

import java.util.Scanner;

//Ceci est l'équivalent de la classe SaisieRPN

public class Launcher {
	
	public static void main(String args[]) 
	{
		int number = 0;
		Calcul c = new Calcul();
		Scanner sc = new Scanner(System.in);
		String str = "";
		
		System.out.println("saisissez un chiffre ou une opérande. Saisissez exit pour quitter. : ");
		
		//boucle permanente
		while(true)
		{
			//On récupère l'input
			str = sc.nextLine();
			
			//Si exit, on sort de la boucle
			if(str.equals("exit"))
			{
				break;
			}
			//si c'est bien un nombre
			try
			{
				number = Integer.parseInt(str);
				c.addInt(number);
			}
			//sinon, c'est une opérande
			//Peut être qu'utiliser les blocs "try catch" pour ce genre d'opération
			//(le "catch" n'étant pas vraiment une erreur en soi mais plutôt un "else")
			//n'est pas l'option la plus judicieuse pour la clarté du code
			//mais j'expérimente avec le langage, et cela simplifie tout de même la vérification
			//de l'élément passé en paramètre
			catch(NumberFormatException e)
			{
				try
				{
					c.addOp(str);
				}
				catch(NotOperandException e2)
				{
					System.out.println("Le caractère n'est pas une opérande valide.");
				}
				catch(MissingNumberException e3)
				{
					System.out.println("Il faut entrer au moins deux chiffres dans la pile.");
				}
				catch(StringIndexOutOfBoundsException e5)
				{
					System.out.println(" ");
				}
				
			}
			catch(PilePleineException e4)
			{
				System.out.println("La pile est pleine. L'élément n'a pas été ajouté.");
			}

		}
		sc.close();
	}
}
