package td_npi;

public class NotOperandException extends Exceptions {
	
	public NotOperandException() 
	{
		super("Le caractère n'est pas une opérande valide.");
	}
}
