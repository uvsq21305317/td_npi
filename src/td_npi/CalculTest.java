package td_npi;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculTest {

	/* @Before
	public void setUp() throws Exception 
	{
	    Calcul c = new Calcul();
	} */
	//Before pas reconnu
	
	@Test
	public void TestAddInt() throws Exception 
	{
		Calcul c = new Calcul();
		c.addInt(2);
		assertEquals(2, c.pile[c.pointeur]);
		
	}
	
	@Test
	public void TestAddition() throws Exception
	{
		Calcul c = new Calcul();
		c.addInt(2);
		c.addInt(2);
		c.addInt(2);
		c.addInt(2);
		c.addOp("+");
		c.addOp("-");
		c.addOp("*");
		assertEquals(-4, c.pile[c.pointeur]);
		
	}
	
	@Test(expected = ArithmeticException.class)
	public void TestDivZero() throws Exception 
	{
		Calcul c = new Calcul();
		c.addInt(2);
		c.addInt(0);
		c.addOp("/");
		fail();
		//SI on arrive ici c'est que la division par zéro a réussie
		
	}
	
	@Test(expected = NotOperandException.class)
	public void TestRandomInput() throws Exception 
	{
		Calcul c = new Calcul();
		c.addInt(2);
		c.addInt(6);
		c.addOp("g");
		fail();
		//SI on arrive ici c'est que l'opération "2g6" a réussie
		
	}
	

	
	

}
