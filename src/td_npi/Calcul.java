package td_npi;

//Ceci est l'équivalent de la classe "MoteurRPN"

public class Calcul {

	    final int SIZE_MAX = 1000; 	//taille max pile
		int[] pile;					//pile d'entiers
		int pointeur;				//position du sommet de la pile
		
		//constructeur
		public Calcul()
		{
			
			this.pile =  new int[SIZE_MAX];
			this.pointeur = -1;
		}
		
		//affiche la pile de chiffre actuelle
		public void getPile()
		{
			System.out.println("pile actuelle : ");
			for (int i=0; i<=pointeur; i++)
			{
			  System.out.println(pile[i] + " ");
			}
		}
		
		//Ajoute un entier dans la pile
		public boolean addInt(int n) throws PilePleineException
		{
			if(pointeur < SIZE_MAX-1)
			{
				pointeur++;
				pile[pointeur] = n;
				this.getPile();
				return true;
			}
			this.getPile();
			throw new PilePleineException();

		}
		
		//Récupère l'opérande et exécute le calcul approprié puis stocke le résultat au sommet de la pile
		public void addOp(String s) throws NotOperandException, MissingNumberException, ArithmeticException
		{
			if(s.length() > 1)
			{
				System.out.println("Attention : plus d'un caractère détécté. Seul le premier caractère sera prit en compte.");
			}
			if(pointeur < 1)
			{
				throw new MissingNumberException();
			}
			
			char op = s.charAt(0);
			switch(op)
			{
				case '+' : 	pile[pointeur - 1] = Operation.PLUS.eval(pile[pointeur-1], pile[pointeur]); pointeur--; this.getPile(); break;
				case '-' : 	pile[pointeur - 1] = Operation.MOINS.eval(pile[pointeur-1], pile[pointeur]); pointeur--; this.getPile(); break;
				case '*' : 	pile[pointeur - 1] = Operation.MULT.eval(pile[pointeur-1], pile[pointeur]); pointeur--; this.getPile(); break;
				case '/' : 	try {
									pile[pointeur - 1] = Operation.DIV.eval(pile[pointeur-1], pile[pointeur]); 
									pointeur--; 
									this.getPile(); 
								}
							catch(ArithmeticException e)
								{
									throw new ArithmeticException("division par zéro.");
								}
							break;
				default : throw new NotOperandException();
			}
		}
	}
