package td_npi;

public enum Operation {
	
	
	PLUS('+') {
		@Override
		public int eval(int a, int b) {
			return a+b;
		}
	}, MOINS('-') {
		@Override
		public int eval(int a, int b) {
			return a-b;
		}
	}, MULT('*') {
		@Override
		public int eval(int a, int b) {
			return a*b;
		}
	}, DIV('/') {
		@Override
		public int eval(int a, int b) {
			return a/b;
		}
	} ;
	abstract public int eval(int a, int b);
	
	
	char symbole;
	
	private Operation(char symbole)
	{
		this.symbole = symbole;
	}
}

	

